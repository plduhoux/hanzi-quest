## HSK

hsk.json is from https://github.com/gigacool/hanyu-shuiping-kaoshi/tree/master

Duolingo word is from here : https://github.com/anki-decks/anki-deck-for-duolingo-chinese/blob/master/words.tsv

Quiz hanzi is from https://hanziwriter.org/docs.html#loading-character-data-link

## Setup

Make sure to install the dependencies:

```bash
# pnpm
pnpm install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# pnpm
pnpm run dev
```
