// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ["unplugin-icons/nuxt", '@nuxtjs/tailwindcss', "@pinia/nuxt", 'pinia-plugin-persistedstate/nuxt'],

  shadcn: {
    /**
     * Prefix for all the imported component
     */
    prefix: '',
    /**
     * Directory that the component lives in.
     * @default "./components/ui"
     */
    componentDir: './components/ui'
  },

  app: {
    head: {
      title: "Hanzi Quest",
      htmlAttrs: {
        lang: "en",
      },
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        { hid: "description", name: "description", content: "" },
        { name: "format-detection", content: "telephone=no" },
        {
          name: "title",
          content: "Hanzi Quest",
        },
        {
          property: "og:url",
          content: "https://hanziquest.plduhoux.fr",
        },
        {
          property: "og:title",
          content: "Hanzi Quest",
        },
        {
          property: "og:description",
          content:
            "Learn chinese character by drawing random characters you should know",
        },
        /*{
          property: "og:image",
          content: "https://plduhoux.fr/mebg.jpeg",
        },*/
        /*{
          property: "twitter:card",
          content: "summary_large_image",
        },*/
        {
          property: "twitter:url",
          content:"https://hanziquest.plduhoux.fr",
        },
        {
          property: "twitter:title",
          content: "Hanzi Quest",
        },
        {
          property: "twitter:description",
          content:
          "Learn chinese character by drawing random characters you should know",
        },
        /*{
          property: "twitter:image",
          content: "https://plduhoux.fr/mebg.jpeg",
        },*/
      ],
      link: [
        {
          rel: "apple-touch-icon",
          sizes: "180x180",
          href: "/apple-touch-icon.png",
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: "/favicon-32x32.png",
        },
        {
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: "/favicon-16x16.png",
        },
        { rel: "manifest", href: "/site.webmanifest" },
      ],
    },
  },

  compatibilityDate: "2024-09-14",
})