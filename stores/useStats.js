import { defineStore } from "pinia";

export const useStats = defineStore("stats", {
  state: () => ({
    hanzi: {} /* {hanzi: {seen: int, grade: number, grade_blind: number}} */,
  }),
  persist: true,
  actions: {
    finishHanzi(hanzi, mistakes, blind) {
      if (!this.hanzi[hanzi]) this.hanzi[hanzi] = { seen: 0 };
      this.hanzi[hanzi].seen++;
      let grade = 100 - mistakes * 5;
      if (blind) {
        if (this.hanzi[hanzi].grade_blind != null) {
          this.hanzi[hanzi].grade_blind = Math.ceil(
            (this.hanzi[hanzi].grade_blind + grade) / 2
          );
        } else {
          this.hanzi[hanzi].grade_blind = grade;
        }
      } else {
        if (this.hanzi[hanzi].grade != null) {
          this.hanzi[hanzi].grade = Math.ceil(
            (this.hanzi[hanzi].grade + grade) / 2
          );
        } else {
          this.hanzi[hanzi].grade = grade;
        }
      }
    },
  },
});
